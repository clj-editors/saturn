(ns saturn.all-tests
  (:require [clojure.test :as test]
            [saturn.ui.tests :as tests]
            [check.core :refer [check]]

            [saturn.adapter.atom-test]
            [saturn.rings.services-test]
            [saturn.rings.layout-provider-test]
            ["electron" :refer [ipcRenderer]]))

(defmethod test/report [::test/default :begin-test-var] [m]
  (swap! tests/state #(-> %
                          (assoc-in [:test (:var m)] {:ns-name (test/testing-vars-str m)
                                                      :state :begin})
                          (update :in-order conj {:var (:var m)}))))

(defmethod test/report [::test/default :end-test-var] [m]
  (swap! tests/state assoc-in [:test (:var m) :state] :end)
  (println "Testing:" (test/testing-vars-str m)))

(defn- make-ctx-and-get-var []
  (let [ctx (test/testing-contexts-str)
        var (-> @tests/state :in-order peek :var)]
    (when (-> @tests/state (get-in [:test var :ctx]) (not= ctx))
      (swap! tests/state #(-> %
                              (assoc-in [:test var :ctx] ctx)
                              (update :in-order conj {:var var :ctx ctx}))))
    var))

(defmethod test/report [::test/default :pass] [m]
  (swap! tests/state update :in-order conj {:var (make-ctx-and-get-var)
                                            :test (assoc m :pass? true)})
  (println "\n  \033[0;32mPassed" (test/testing-vars-str m) "\033[0;0m" (:expected m)))

(defmethod test/report [::test/default :fail] [m]
  (swap! tests/state update :in-order conj {:var (make-ctx-and-get-var)
                                            :test (assoc m :pass? false)})
  (println "\n  \033[1;31mFailed" (test/testing-vars-str m) "\033[0;0m" (:actual m)))

(defmethod test/report [::test/default :error] [m]
  (swap! tests/state update :in-order conj {:var (make-ctx-and-get-var)
                                            :test (assoc m :pass? false)})
  (println "\n  \033[1;35mCrashed" (test/testing-vars-str m) "\033[0;0m" (:actual m)))

(defmethod test/report [::test/default :summary] [m]
  (println "\nRan" (:test m) "tests containing"
           (+ (:pass m) (:fail m) (:error m)) "assertions.")
  (println (:pass m) "passes," (:fail m) "failures," (:error m) "errors.")
  (when js/process.env.CI
    (.send ipcRenderer "close" (+ (:fail m) (:error m)))))

(defn- ^:dev/after-load run-tests! []
  (reset! tests/state {:in-order [] :only-failed? (:only-failed? @tests/state)})
  (test/run-all-tests #"^saturn.*-test"))
  ; (promesa.core/do!
  ;  (promesa.core/delay 1000)
  ;  (reagent.dom/force-update-all)))
