(ns saturn.adapter.atom-test
  (:require [clojure.test :refer [deftest]]
            [check.async :refer [async-test check testing]]
            [promesa.core :as p]))

(deftest atom-text-editor
  (async-test "opening a file on the editor"
    (p/let [^js editor (.. js/atom -workspace (open "test/saturn/adapter/atom_test.cljs"
                                                    #js {:initialLine 2
                                                         :initialColumn 6}))]
      (check (.getText editor) => #"saturn\.adapter\.atom\-test")
      (check (.getWordUnderCursor editor) => "require"))))
