(ns saturn.rings.layout-provider-test
  (:require [clojure.test :refer [deftest]]
            [promesa.core :as p]
            [saturn.events :as events]
            [re-frame.frame :as re]
            [re-frame.context :as ctx]
            [check.async :refer [async-test testing check]]))

(defn- mock-presentable [frame presentable-name result]
  (re/dispatch frame [:rings.provider/register
                      "presentable"
                      (constantly
                       #js {:kind (name presentable-name)
                            :constructor (constantly result)})]))

(def frame (events/make-new-frame!))
(deftest open-something-on-center-by-default
  (async-test "opening something and layouting it"
    (let [frame (events/make-new-frame!)
          _ (events/register-events! frame)
          first-presentable (p/deferred)
          second-presentable (p/deferred)
          event (atom nil)]
      (mock-presentable frame :something
                        #js {:getElement (constantly (doto (js/document.createElement "div")
                                                           (aset "innerText" "Hello")))})
      (p/do!
       (testing "will generate a presentable thing"
         (re/dispatch frame [:uri/open {:uri "something:///some.thing"
                                        :options {}
                                        :promise first-presentable}])
         (p/then first-presentable #(check (.. ^js % getElement -innerText) => "Hello")))

       (testing "will re-use a presentable if we open the same URI"
         (re/dispatch frame [:uri/open {:uri "something:///some.thing"
                                        :options {}
                                        :promise second-presentable}])
         (p/let [first-opened first-presentable
                 second-opened second-presentable]
           (check first-opened =expect=> second-opened)))))))
