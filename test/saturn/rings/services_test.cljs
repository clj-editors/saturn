(ns saturn.rings.services-test
  (:require [clojure.test :refer [deftest]]
            [re-frame.frame :as re]
            ; [re-frame.context :as ctx]

            [promesa.core :as p]
            [saturn.events :as events]
            [saturn.rings.services :as services]
            [check.async :refer [check async-test testing let-testing]]))

(deftest provider-consumer
  (async-test "registering providers and consumers"
    (testing "provider before consumer"
      (let [frame (events/make-new-frame!)
            _ (services/register-events! frame)
            result (p/deferred)
            provider (constantly {:do (fn [res] (p/resolve! result res))})
            consumer (fn [provider]
                       ((:do provider) :it-works!))]
        (re/dispatch frame [:rings.provider/register "service1" provider])
        (re/dispatch frame [:rings.consumer/register "service1" consumer])
        (check result => :it-works!)))

    (testing "consumer before provider"
      (let [frame (events/make-new-frame!)
            _ (services/register-events! frame)
            result (p/deferred)
            provider (constantly {:do (fn [res] (p/resolve! result res))})
            consumer (fn [provider]
                       ((:do provider) :it-works-too!))]
        (re/dispatch frame [:rings.consumer/register "service1" consumer])
        (re/dispatch frame [:rings.provider/register "service1" provider])
        (check result => :it-works-too!)))))
