const path = require('path')

const exportsPath = path.join(__dirname, 'exports');
require('module').globalPaths.push(exportsPath);
process.env.NODE_PATH = exportsPath;

const {app, BrowserWindow, ipcMain} = require('electron')

app.allowRendererProcessReuse = false
app.on('ready', () => {
  let browser = new BrowserWindow({
    width: 900, height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true
    }}
  )
  browser.loadURL(path.join('file://', __dirname, '/index.html'))
})

ipcMain.on('close', (evt, arg) => {
  console.log("Exiting with status code", arg)
  process.exit(arg)
})
