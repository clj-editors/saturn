(ns saturn.events
  (:require [re-frame.frame :as re]
            [saturn.frame :as s-frame]
            [reagent.core :as r]
            [saturn.schemas :as schemas]
            [promesa.core :as p]
            [malli.dev.pretty :as pretty]
            [saturn.rings.services :as services]

            [saturn.rings.layout-provider :as layout]
            [saturn.rings.editor-provider :as editor]))

(def ^:private initial-state {:saturn.theme/ui "atom-dark-ui"
                              :saturn.theme/syntax "theme-one-dark-syntax"
                              :saturn/services {}
                              :saturn/pending-opens #{}})

(defn- validate-state! [new-state]
  (not (pretty/explain schemas/StateSchema new-state)))

(defn make-new-frame! []
  (let [frame (s-frame/make-new-frame! initial-state)]
    (re/dispatch frame [:theme.ui/change "atom-dark-ui"])
    (set-validator! (:app-db frame) validate-state!)
    frame))

(defn destroy-frame! [frame]
  (re/clear-subscriptions-cache frame))

(defn- open-uri [frame layout {:keys [db]} [_ {:keys [uri params promise] :as open}]]
  (if (string? uri)
    (let [id (str (gensym "presentable-"))
          presentable-prom (.open layout uri params)]
      (p/then presentable-prom (fn [presentable]
                                 (p/resolve! promise presentable)
                                 (re/dispatch frame [:uri/opened id presentable])))
      {:db (update db :saturn/pending-opens conj id)})))

(defn- register-layout [frame provider]
  (re/reg-event-fx frame :uri/open (partial open-uri frame provider)))

(defn- finalize-opening [{:keys [db]} [_ id presentable]])
  ; (prn :OPENED presentable))

(defn register-events! [frame]
  (re/dispatch frame [:rings.consumer/register "layout" #(register-layout frame %)])
  (re/reg-event-fx frame :uri/opened finalize-opening)
  (services/register-events! frame)
  (layout/register-events! frame)
  (editor/register-events! frame))
