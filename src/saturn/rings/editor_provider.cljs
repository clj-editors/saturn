(ns saturn.rings.editor-provider
  (:require [re-frame.frame :as re]
            [promesa.core :as p]
            [clojure.string :as str]
            ["@monaco-editor/loader$default" :as loader]
            ["fs" :as fs]))

(def ^:private fs-promise (.-promises fs))

(def ^:private default-params
  #js {:automaticLayout true
       :lineNumbers "on"
       :theme "vs-dark"
       :renderLineHighlight "all"
       :minimap #js {:enabled false}})

(defn- remove-command-palette! [^js editor, ^js elem, ^js mon]
  (.addCommand editor (.. mon -KeyCode -F1) (constantly nil))
  (.onContextMenu editor (fn [_]
                           (let [root (.. elem
                                          (querySelector ".shadow-root-host")
                                          -shadowRoot
                                          (querySelector ".actions-container"))
                                 children (.-childNodes root)]

                             (doseq [idx (range (.-length children))
                                     :let [item (aget children idx)
                                           txt (.-innerText item)]
                                     :when (= txt "Command Palette")]
                               (.removeChild root item)
                               (.removeChild root (aget children (dec idx))))))))

(defn- open-in-monaco! [uri options]
  (p/let [^js mon (.init loader)
          elem (js/document.createElement "text-editor")
          ^js editor (.. mon -editor (create elem default-params))
          text (. fs-promise readFile uri)
          text (str text)
          uri (.. mon -Uri (file uri))
          model (or (.. mon -editor (getModel uri))
                    (.. mon -editor (createModel text nil uri)))
          position (cond-> {}
                     (.-initialLine options) (assoc :lineNumber (.-initialLine options))
                     (.-initialColumn options) (assoc :column (.-initialColumn options)))
          js-pos (clj->js position)]

          ; Stupid monaco does not have a way to remove command palette...
    (.setValue model text)
    (.setModel editor model)
    (remove-command-palette! editor elem mon)
    (when (seq position)
      (. editor setPosition js-pos)
      (. editor revealPositionInCenterIfOutsideViewport js-pos))
    #js {:getElement (constantly elem)
         :title (str/replace (str uri) #".*[/\\]" "")
         :monaco editor}))

(defn- provide-editor []
  #js {:kind "text-editor"
       :constructor open-in-monaco!})

(defn register-events! [frame]
  (re/dispatch frame [:rings.provider/register "presentable" provide-editor]))
