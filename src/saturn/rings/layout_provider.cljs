(ns saturn.rings.layout-provider
  (:require [re-frame.frame :as re]
            [promesa.core :as p]
            [clojure.string :as str]
            [saturn.ui.tabs :as tabs]
            ["react" :as react]
            [reagent.core :as r]
            [saturn.frame :as s-frame]))

(defn- function->react [elem props function]
  (let [[fun fun-set!] (.useState react)
        ref (.useRef react nil)]
    (. react useEffect (fn []
                         (when-let [e (and (not fun) (.-current ref))]
                           (p/let [res (function e)]
                             (fun-set! res))))
                #js [(.-current ref)])
    (. react createElement elem (-> props (assoc :ref ref) clj->js))))

(defn- react-elem [frame]
  (let [centers (re/subscribe frame [:center/presentables])
        center (re/subscribe frame [:center/active])
        ^js p-center (:presentable @center)]
    [:atom-pane.pane.active
     [tabs/tabview frame :center @centers @center]
     (when p-center
       (if (.-getReact p-center)
         [:> (.getReact p-center)]
         [:f> #(function->react "div" {:className "item-views"}
                                (fn [e]
                                  (.appendChild e (.getElement p-center))))]))]))

(defn- include-into-elem [layouts presentable position uri]
  (let [size (count (get-in layouts [:layouts position :presentables]))]
    (-> layouts
        (update-in [:layouts position :presentables] conj {:presentable presentable
                                                           :uri uri
                                                           :title (.-title presentable)})
        (assoc-in [:layouts position :active] size))))

(defn- open [frame uri params]
  (p/let [[_ protocol] (re-find #"(.+?)://" uri)
          db @(:app-db frame)
          creator (or (-> db :presentables-providers (get protocol))
                      (-> db :presentables-providers (get "text-editor")))
          position :center
          found (->> db :layouts position :presentables
                     (zipmap (range))
                     (filter (fn [[idx elem]] (-> elem :uri (= uri))))
                     first)]
    (if found
      (do
        (swap! (:app-db frame) assoc-in [:layouts position :active] (first found))
        (-> found second :presentable))
      (p/let [presentable (creator uri (or params #js {}))]
        (swap! (:app-db frame) include-into-elem presentable position uri)
        presentable))))

(defn provider [frame]
  #js {:open (partial open frame)
       :getReact #(r/as-element [react-elem frame])})

(defn- register-presentable [frame provider]
  (swap! (:app-db frame) assoc-in [:presentables-providers (.-kind provider)] (.-constructor provider)))

(defn- activate-tab [db [_ pos idx]]
  (assoc-in db [:layouts pos :active] idx))

(defn register-events! [frame]
  (let [new-frame (s-frame/make-new-frame! {:presentables-providers {}
                                            :layouts {:center {:presentables []}
                                                      :left {:presentables []}
                                                      :right {:presentables []}
                                                      :bottom {:presentables []}}
                                            :active :center})]
    (re/reg-event-db new-frame :tab/activate activate-tab)
    (re/dispatch frame [:rings.provider/register "layout" #(provider new-frame)])
    (re/dispatch frame [:rings.consumer/register "presentable"
                        #(register-presentable new-frame %)])
    (doseq [pos [:center :left :right :bottom]]
      (re/reg-sub new-frame
                  (keyword (name pos) "presentables")
                  [#(get-in %1 [:layouts pos :presentables])])
      (re/reg-sub new-frame
                  (keyword (name pos) "active")
                  [#(let [active (get-in %1 [:layouts pos :active])]
                      (get-in %1 [:layouts pos :presentables active]))]))))
