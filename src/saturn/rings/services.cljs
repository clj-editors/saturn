(ns saturn.rings.services
  (:require [re-frame.frame :as re]))

(def EvtSchema [:tuple any? string? fn?])

(def ^:private sconj (fnil conj #{}))

(defn- register-provider!
  {:malli/schema [:=> [:cat int? EvtSchema] keyword?]}
  [{:keys [db]} [_ service-name constructor]]

  (let [provider (constructor)]
    {:db (update-in db [:saturn/services service-name :providers] sconj provider)
     :saturn.consumer/trigger-all [service-name provider]}))

(defn- register-consumer!
  {:malli/schema [:=> [:cat int? EvtSchema] keyword?]}
  [{:keys [db]} [_ service-name callback]]
  {:db (update-in db [:saturn/services service-name :consumers] sconj callback)
   :saturn.consumer/trigger [service-name callback]})

(defn- trigger-consumer! [[service-name callback] {:keys [app-db]}]
  (doseq [provider (get-in @app-db [:saturn/services service-name :providers])]
    (callback provider)))

(defn- trigger-all-consumers! [[service-name provider] {:keys [app-db]}]
  (doseq [callback (get-in @app-db [:saturn/services service-name :consumers])]
    (callback provider)))

(defn register-events! [frame]
  (re/reg-event-fx frame :rings.provider/register register-provider!)
  (re/reg-event-fx frame :rings.consumer/register register-consumer!)
  (re/reg-fx frame :saturn.consumer/trigger trigger-consumer!)
  (re/reg-fx frame :saturn.consumer/trigger-all trigger-all-consumers!))
