(ns saturn.adapter.atom
  (:require [promesa.core :as p]
            [saturn.adapter.atom.text-editor :as te]
            [shadow.cljs.modern :as modern]
            [re-frame.frame :as re]))

(defonce frame (atom nil))
(defn- generate-spy
  ([] (generate-spy {}))
  ([passthrough]
   (let [calls (atom {})
         target {:calls calls :passthrough passthrough}
         spy #js {:get (fn [target prop receiver]
                         (if (contains? passthrough (keyword prop))
                           (get passthrough (keyword prop))
                           (let [{:keys [spies return]} (generate-spy)]
                             (swap! (:calls target) assoc (keyword prop) spies)
                             (constantly return))))}]
     {:spies calls
      :return (js/Proxy. target spy)})))

(defn- open-things [uri params]
  (when @frame
    (let [p (p/deferred)]
      (re/dispatch @frame [:uri/open {:uri uri :params params :promise p}])
      (p/then p te/adapt))))

;; FIXME: This pane thing
(defonce center-pane (atom nil))
(set! *warn-on-infer* false)
(modern/defclass Pane
  (constructor [this])

  Object
  (getElement [this] @center-pane)
  (getContainer [this] #js {})
  (getItems [this] #js [])
  (onDidDestroy [this callback] #js {:dispose (constantly nil)}))

(modern/defclass WorkspaceCenter
  (constructor [this])
  Object
  (onDidChangeActivePaneItem [this callback] #js {:dispose (constantly nil)})
  (observePanes [this callback]
                (prn :OBSERVING-PANE)
                (callback (new Pane))
                #js {:dispose (constantly nil)}))
(set! *warn-on-infer* true)

(def ^:private workspace (generate-spy {:open open-things
                                        :getCenter (fn []
                                                     (new WorkspaceCenter))}))
                                                     ; @center-pane)}))
(def ^:private config (generate-spy))
(def ^:private notifications (generate-spy))
(def ^:private commands (generate-spy))
(def ^:private keymaps (generate-spy {:onDidLoadUserKeymap (constantly #js {:dispose #()})}))

;; Unfortunately, Atom expects both an exports and a global var with different values...
(set! js/global.atom
  (clj->js {:workspace (:return workspace)
            :config (:return config)
            :project {:getPaths (constantly #js ["."])
                      :getRepositories (constantly #js [])
                      :relativizePath (fn [e] e)
                      :relative (fn [e] e)
                      :removePath (constantly nil)
                      :onDidChangePaths (constantly #js {:dispose #()})}
            :notifications (:return notifications)
            :commands (:return commands)
            :styles {:onDidAddStyleElement (constantly #js {:dispose (constantly nil)})
                     :onDidRemoveStyleElement (constantly #js {:dispose (constantly nil)})
                     :onDidUpdateStyleElement (constantly #js {:dispose (constantly nil)})}
            :keymaps (:return keymaps)}))
