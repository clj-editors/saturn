(ns saturn.adapter.atom.text-editor
  (:require [promesa.core :as p]))

(defn adapt [^js presentable]
  (def presentable presentable)
  (let [monaco (.-monaco presentable)]
    #js {:getText #(.getValue monaco)
         :getWordUnderCursor #(.. monaco
                                  getModel
                                  (getWordAtPosition (.. monaco getPosition))
                                  -word)}))
