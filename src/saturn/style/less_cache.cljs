(ns saturn.style.less-cache
  (:require [promesa.core :as p]
            ["crypto" :as crypto]
            ["path" :as path]
            ["less/lib/less-node/index$default" :as less]
            ["fs" :as fs]))

(defn parse-file [file opts]
  (p/let [file (path/resolve file)
          contents (fs/readFileSync file "utf-8")
          result (less/render contents (clj->js (assoc opts :filename file)))]
    (.-css result)))

(defn parse-style [style-path]
  (parse-file (path/join "styles" style-path "index.less")
              {:paths [(path/resolve (path/join "styles" "variables"))]
               :math 0}))

(defn digest-for-content [content]
  (.. crypto
      (createHash "SHA1")
      (update content "utf8")
      (digest "hex")))
