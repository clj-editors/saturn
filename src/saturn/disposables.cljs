(ns saturn.disposables)

(defonce ^:private disposables (atom []))
(defn add-disposable! [disposable]
  (when js/goog.DEBUG
    (swap! disposables conj disposable)))

(defn- ^:dev/before-load stop-all! []
  (doseq [disposable @disposables]
    (.dispose disposable)))
