(ns saturn.frame
  (:require [re-frame.frame :as re]
            [re-frame.fx :as fx]
            [re-frame.cofx :as cofx]))

(defn make-new-frame! [initial-state]
  (let [frame (re/make-frame)]
    (fx/register-built-in! frame)
    (cofx/register-built-in! frame)
    (when (empty? @(:app-db frame)) (reset! (:app-db frame) initial-state))
    frame))

(defn destroy-frame! [frame]
  (re/clear-subscriptions-cache frame))
