(ns saturn.view
  (:require [re-frame.frame :as re]
            [promesa.core :as p]

            [reagent.core :as r]
            [reagent.dom :as r-dom]
            ["react" :as react]

            [saturn.disposables :refer [add-disposable!]]
            [saturn.style :as style]
            [saturn.adapter.atom :as atom]
            [saturn.ui.widgets :as w]
            [saturn.ui.tests :as tests]
            [saturn.events :as events]

            ["@monaco-editor/loader$default" :as loader]
            ["../atom/tree-view/lib/main.js" :as tree-view]))

(defn- async-monaco! [elem params]
  (p/let [^js monaco (.init loader)]
    (.. monaco
        -editor
        (create elem (clj->js params)))))

(def left-dock
  (do
    (.activate tree-view)
    (let [d (w/create-dock! (doto (.. tree-view getTreeViewInstance -element)
                                  (.. -style (setProperty "flex-grow" "1")))
                          {:location "left"
                           :size 245
                           :visible true
                           :hidable? true
                           :didActivate (constantly nil)})]
      d)))

(defn- html->react [elem props html]
  (let [ref (. react useRef nil)]
    (. react useEffect
      (fn [] (when-let [e (.-current ref)] (.appendChild e html)))
      #js [(.-current ref)])
    (. react createElement elem (-> props (assoc :ref ref) clj->js))))

(defn- function->react [elem props function]
  (let [[fun fun-set!] (.useState react)
        ref (.useRef react nil)]
    (. react useEffect (fn []
                         (when-let [e (and (not fun) (.-current ref))]
                           (p/let [res (function e)]
                             (fun-set! res))))
                #js [(.-current ref)])
    (. react createElement elem (-> props (assoc :ref ref) clj->js))))

(declare editor)

(defonce _ui-style
  (-> "head"
      (js/document.querySelector)
      (.appendChild (.buildStylesElement style/styles))))

(defn- test-ui []
  (let [div (doto (js/document.createElement "div")
                  (aset "innerText" "Hello, world")
                  (.. -style (setProperty "flex-grow" "1")))
        d (w/create-dock! div
                        {:location "bottom"
                         :size 300
                         :visible true
                         :hidable? true
                         :didActivate (constantly nil)})]
    d))

; (def center-pane (doto (js/document.createElement "atom-pane")
;                        (.. -classList (add "pane" "active"))))

(def CenterPane
  (r/create-class {:constructor (fn [ & args]
                                  (prn :ARGS args))
                   :reagent-render (fn [ & args]
                                     (prn :RENDER args)
                                     [:atom-pane.pane.active "HELLO"])}))
(defn center-pane [ & children]
  (let [ref (. react useRef)]
    (. react useEffect
      (fn []
        (when-let [e (.-current ref)]))

      #js [(.-current ref)])
    (r/as-element
     [:atom-pane.pane.active {:ref ref} children])))

(defonce ^:private layout-react (r/atom nil))
(defn editor-ui []
  (let [elem (.getElement left-dock)]
    [:atom-workspace-axis {:class "horizontal"}
     [:f> #(html->react "atom-panel-container"
                        {:class (.-className elem)}
                        elem)]
     [:atom-workspace-axis {:class "vertical"}
      [:atom-pane-container {:class "pane"}
       @layout-react]]]))

(defn ui [frame]
  (let [ui-theme (re/subscribe frame [:theme.ui/name])
        themes ["theme-one-dark-syntax" (str "theme-" @ui-theme)]]
    [:atom-workspace {:class (into themes ["workspace" "scrollbars-visible-always"])
                      :tabindex "-1"}
     (if @tests/run-tests?
       [:atom-workspace-axis {:class "vertical"}
        [editor-ui]
        [tests/ui]]
       [editor-ui])]))

(defn- listen-to-layouts [^js provider]
  (reset! layout-react (.getReact provider)))

(defn ^:dev/after-load main []
  (let [frame (events/make-new-frame!)]
    (re/dispatch frame [:rings.consumer/register "layout" listen-to-layouts])

    (reset! atom/frame frame)
    (.config loader (clj->js {:paths {:vs "./node_modules/monaco-editor/dev/vs"}}))
    (events/register-events! frame)
    (style/define-styles! frame)
    (re/dispatch frame [:theme.ui/change "atom-dark-ui"])
    (r-dom/render
     [ui frame]
     (js/document.querySelector "div#app"))
    (when js/process.env.CI
      (p/do!
       (p/delay 1000)
       (saturn.all-tests/run-tests!)))))
