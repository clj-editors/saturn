(ns saturn.schemas
  (:require [malli.util :as mu]))

(def ServicesSchema
  [:map-of string? [:map
                     [:providers {:optional true} [:set any?]]
                     [:consumers {:optional true} [:set fn?]]]])

(def PresentableSchema
  [:map
   [:presentable/uri string?]
   [:presentable/title string?]
   [:presentable/meta [:map-of any? any?]]
   [:presentable/component any?]])

(def StateSchema
  (mu/closed-schema
   [:map
    [:saturn.theme/ui string?]
    [:saturn.theme/syntax string?]
    [:saturn/services ServicesSchema]
    [:saturn/pending-opens [:set string?]]]))
