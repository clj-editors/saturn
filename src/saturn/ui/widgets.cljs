(ns saturn.ui.widgets
  (:require [reagent.core :as r]
            [reagent.dom :as r-dom]
            ["react" :as react]
            ["../../js-deps/panel/dock" :as Dock]))

(defn- html->react [elem props html]
  (let [ref (. react useRef nil)]
    (. react useEffect
      (fn [] (when-let [e (.-current ref)] (.appendChild e html)))
      #js [(.-current ref)])
    (. react createElement elem (-> props (assoc :ref ref) clj->js))))

(defn create-dock! [item params]
  (let [dock (new Dock (clj->js (assoc params
                                       :viewRegistry {:getView (constantly item)})))]
    (set! (.. dock getElement -style -minWidth) "2px")
    (when-let [size (:size params)] (.. dock (setState #js {:size size})))
    (when (:hidable? params)
      (set! (.. dock getElement -onmouseenter)
        #(.. dock getElement (querySelector ".atom-dock-toggle-button")
             -classList (add "atom-dock-toggle-button-visible")))
      (set! (.. dock getElement -onmouseleave)
        #(.. dock getElement (querySelector ".atom-dock-toggle-button")
             -classList (remove "atom-dock-toggle-button-visible"))))
    (when-let [visible (:visible params)]
      (.show dock))
    dock))

(defn- atom-dock [props react-elem]
  (let [div (js/document.createElement "div")
        elem (create-dock! div props)]
    (r-dom/render react-elem div)
    (.. div -style (setProperty "flex-grow" "1"))
    [:f> #(html->react "atom-panel-container"
                       {:class "panes"}
                       (.getElement elem))]))
