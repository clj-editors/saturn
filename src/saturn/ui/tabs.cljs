(ns saturn.ui.tabs
  (:require [re-frame.frame :as re]))

(defn tabview [frame pos presentables current]
  (->> presentables
       (map (fn [idx p] [:li {:class ["tab" "sortable" (when (= p current) "active")]}
                         [:div.title {:on-click (fn [e] (re/dispatch frame [:tab/activate pos idx]))}
                          (:title p)]
                         [:div.close-icon]])
            (range))
       (into [:ul {:class "list-inline tab-bar inset-panel" :location "center"}])))
