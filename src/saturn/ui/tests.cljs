(ns saturn.ui.tests
  (:require [saturn.ui.widgets :as w]
            [reagent.core :as r]
            [promesa.core :as p]
            [clojure.test :as test]
            ["path" :as path]
            ["ansi_up" :default Ansi]))

(defn- coerce-env [env]
  (not (contains? #{"" "false" "0" nil} env)))

(defonce run-tests? (r/atom (or (coerce-env js/process.env.TESTS)
                                (coerce-env js/process.env.CI))))

(defonce state (r/atom {:only-failed? true}))

(defn- test-ui []
  (let [div (doto (js/document.createElement "div")
                  (aset "innerText" "Hello, world")
                  (.. -style (setProperty "flex-grow" "1")))
        d (w/create-dock! div
                        {:location "bottom"
                         :size 300
                         :visible true
                         :hidable? true
                         :didActivate (constantly nil)})]
    d))

(def curr-dir js/__dirname)
(defn- goto-file [test]
  (fn [^js evt]
    (.preventDefault evt)
    (.stopPropagation evt)
    (.. js/atom -workspace (open (path/join curr-dir "test" (:file test))
                                 #js {:initialLine (:line test)
                                      :initialColumn (:column test)}))))

(defonce ansi (new Ansi))
(defn- test-result [test]
  (let [pass? (:pass? test)
        color (if pass? "#4F4" "#F55")
        message (if pass? :expected :actual)
        html (. ansi ansi_to_html (-> test message str))]

    [:pre {:style {:color color}}
     [:div {:dangerouslySetInnerHTML #js {:__html html}}]
     [:a {:href "#" :on-click (goto-file test) :style {:color color :opacity "0.4"}}
      (str (:file test) ":" (:line test) ":" (:column test))]]))

(defn- suite-ui []
  [:div {:style {:margin "1em"
                 :overflow "scroll"
                 :height "100%"}}
   [:h2 "Tests"]
   [:div
    [:label "Only failed tests " [:input {:type "checkbox"
                                          :on-change #(swap! state update :only-failed? not)
                                          :checked (:only-failed? @state)}]]]
   (->> (range)
        (map (fn [{:keys [var ctx test]} idx]
               (if (:only-failed? @state)
                 (when (and test (-> test :pass? not))
                   [:ul {:key idx} [:li {:style {:color "red"}} [test-result test]]])
                 [:<> {:key idx}
                  (cond
                    test [:ul
                          [:li {:style {:color (if (:pass? test) "blue" "red")}}
                           [test-result test]]]
                    ctx [:ul [:i [:strong {:key idx} ctx]]]
                    :else [:h3 (get-in @state [:test var :ns-name])])]))
             (:in-order @state))
        (into [:<>]))])

(defn ui []
  [w/atom-dock {:location "bottom"
                :size 300
                :visible true
                :hidable? true
                :didActivate (constantly nil)}
   [suite-ui]])
