(ns saturn.style
  (:require [saturn.style.less-cache :as less-cache]
            [saturn.disposables :refer [add-disposable!]]
            [promesa.core :as p]
            [re-frame.frame :as re]
            ["path" :as path]
            ["../atom/style-manager/style-manager" :as StyleManager]))

(defonce styles (new StyleManager))

(defonce ui-theme-disposable (atom nil))
(defn- change-theme! [theme]
  (when-let [^js d @ui-theme-disposable] (.dispose d))
  (p/let [theme-css (less-cache/parse-style theme)
          disposable (.addStyleSheet ^js styles theme-css #js {:priority 4})]
    (reset! ui-theme-disposable disposable)
    (add-disposable! disposable)))

(defn- dispatch-change-theme [{:keys [db]} [_ theme]]
  {:db (assoc db :saturn.theme/ui theme)
   :theme.ui/changed theme})

(defonce styles-vars "
  --editor-font-size: 14px;
  --editor-font-family: Hasklig Medium;
  --editor-line-height: 1.8;

ul.tab-bar { cursor: default; }
ul.tab-bar li { cursor: default; }
")

(defn- add-style! [ & params]
  (p/let [contents (less-cache/parse-file
                    (apply path/join params)
                    {:paths [(path/resolve (path/join "styles"))
                             (path/resolve (path/join "styles" "variables"))]
                     :math 0})
          disposable (.addStyleSheet ^js styles contents #js {:priority 3})]
    (add-disposable! disposable)))

(defn- default-styles! []
  (-> styles (.addStyleSheet styles-vars #js {:priority 2}) add-disposable!)
  (add-style! "styles" "atom.less")
  (add-style! "styles" "tabs.less"))

(defn- define-styles! [frame]
  (default-styles!)
  (re/reg-sub frame :theme.ui/name [#(:saturn.theme/ui %1)])
  (re/reg-event-fx frame :theme.ui/change dispatch-change-theme)
  (re/reg-fx frame :theme.ui/changed change-theme!))
