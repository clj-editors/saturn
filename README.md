# Saturn

Big, but less dense, hackabe code editor. Reconstructed from Atom fragments

## Installing and setting things up...

##### Make sure that you have installed in your machine
- Clojure
- nodeJS
- [shadow-cljs](https://shadow-cljs.github.io/docs/UsersGuide.html#_installation)

##### Compiling
```bash
$ npm install
$ npx shadow-cljs watch view
$ npm start
```

## Development Ideas

Saturn's priorities are in the following order:

1. Be hackable to the core (less than Emacs, more than VSCode)
1. Have a plug-in architecture that's compatible with most (but maybe not all) plug-ins from Atom and VSCode
1. Be easy to build and develop. This means:
    1. No "install these four build tools and then run these 5 commands to run the code
    1. No "reinvent the wheel" and instead reuse most, if not all, plug-ins and UIs that exist elsewhere
    1. Avoid binary dependencies/npm modules, and possibly have none (WASM-based alternatives may be a good fit here, even if they are slight worse)
    1. Have a HUGE focus on plug-ins. So, "core" things like a file watcher could be hot-replaced for faster versions without any "recompiling" of the editor
1. Be usable in all moments. This also translates to "do not spend five years developing version 0.0.1".
    1. This can mean that first versions may be buggy. It's a pitty, but that's how open-source works
    1. But this also means that the project is truly "open" - anyone can use any version with ease
1. Be well-tested. Probably avoid unit-testing that tests internal state, and instead focus on the architecture and higher-level constructions. The reason is quite simple: the editor is intented to be **fully configurable**. This translates also to all code being **fully repleaceble**. As an example, we're currently using re-frame for event dispatch. It should be possible to replace re-frame with other things, and make sure that higher-level tests still pass if the code is correct
    1. This is easier said than done, and some internal-structure test will be inevitable
    1. But this also means do not use Jest's snapshot testing (or a ClojureScript version of it) or "selector" testing (checking if a HTML element with a specific property exist).
    1. If possible, it's a good idea to use Selenium or some other E2E testing library.

Main document: [Development Ideas](doc/dev-ideas.md)
