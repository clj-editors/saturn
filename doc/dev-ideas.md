# Development Ideas

Saturn is an editor that have the same thing that Adam did in the past - be a hackable text editor. And that's where things get complicated - because Atom have a very, very old codebase with not lots of "not invented here" elements and libraries - for example, the files watcher, the text fuzzy matcher, some filesystem APIs (`fs-plus`), log of deprecated elements, text editor component, an _even the UI library_ that makes the text editor was created by the Atom team, which means that it's close to impossivel to maintain Atom...

But also, generating a new editor from scratch is really complicated. So one of the goals of Saturn is to reuse what other editors already have as much as possible. For example, some plugins in Atom are probably reusable. So it's interesting to reuse most of the plugins if we can - for example, instead of generating a new editor component, with new syntax highlighting rules and everything, Saturn reuses Monaco editor (the same that VSCode uses). The only problem with this approach is that this elements do not communicate well sometimes - so we can say that Saturn it's mostly "facade" or "adapter" patterns - in the sense that we are adapting one element/plug-in to another.

Saturn is also developed in ClojureScript (because of the hot reload capabilities and fast inspection of code) and also because it's one of the languages that I'm most comfortable with. The problem with ClojureScript is that most Atom and VSCode plug-ins are in Javascript, and as Saturn will mostly reuse these plug-ins, it is not possible to use ClojureScript-only elements like `vector`s, `list`s, `symbol`s and `keyword`s in the plugin architecture. This also means that Saturn's built-in plugins like the layout, or the Monaco editor itself, can't communicate with Saturn via ClojureScript only -  have to use some kind of API in JavaScript.

That's the most complicated part of this editor - specially because some very interesing "guardrails" libraries like `Mali` or `spec` will not work because they expect ClojureScript objects and not JavaScript ones. We can write a wrapper around this (and it's something that it would be useful want to study a little bit).

## Plug-ins, or "Rings"

Okay, so what exactly is a plug-in in Saturn? In our case, Saturn plugins are called **Rings**. Mostly because Saturn (the planet) would not be as iconic if it didn't have its rings - and the same is true for our editor, too.

Rings, in Saturn, mean something that are external to the "core" editor's code, but orbit around and make the whole editor what it is. One "ring" for example, is the Monaco editor - it's an external library but it's adapter to work inside the editor. Other examples of possible rings are Atom plugins and VSCode plugins - they are made to work on specific editors, but they can be used inside Saturn, so Saturn needs to provide compatible-enough APIs for Atom and VSCode.

Another example of a ring is, for example, fire watchers, and project manager. Saturn will always provide a default option but these default options must be replaceble always. File watchers can be implemented in the simplest way possible (possibly using some Rust code compiled to WASM) even if this means that it will be slower (but this also means that users that want to develop Saturn will not have to bother with setting a C/C++ compiler chain to compile native libraries, and also Electron updates will probably be easier to make). A project manager should be as simple as "remember the open tabs of the editor in this directory".

## Compatible enough

It's not necessary to implement the full API for Atom and VSCode at the beginning - ideally, it only needs to be possible to run LSP-based plug-ins and maybe the webview API (for VSCode). As for Atom, it would be interesting to know which APIs are more used. As this is impossible, it would be great to just implement APIs as it is needed. Another interesting idea is to implement the VSCode API __over__ the Atom one. The reason is that Atom API is _way more_ flexible than the VSCode one, so it may be easier to implement one over the other.

A second possibility is to implement a "Saturn" API (probably based on events/triggers/queries or something like that) and implement both VSCode and Atom APIs over it.

## Release it often, release it fast

Saturn must be in an "usable state" as soon as it is possible. It must be remembered that it is an open-source project with few developers, so it's important that Saturn must be used by these few developers, so they can "feel the pain" of the editor. Also, it also means it should be possible to use Saturn to develop itself.

This also means that most features will be incomplete, inexistent, or buggy in the beginning. Even in this case, it's better than staying 3, 4 years in development, and then publish and discover that some really simple API were not implemented or were implemented incorrectly.

Also, if possible, development should avoid using binary NPM libraries - and the reason for that it's quite simple: it's easier for anyone to develop something if that person does not need to install something, then use some "electron rebuilder" or other hacks in NPM world to rebuild things to Electron. So the idea is to use WASM, maybe with some support from Rust community. But again, these must exist on their own: Atom's mistake of doing everything "in house" must not be forgotten.
